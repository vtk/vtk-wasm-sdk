# Building vtk-wasm image on a local machine

Building the image as the CI does can be done by following these steps:

```sh
# Build the docker image
export VTK_BUILD_ARCHITECTURE=... # wasm32, wasm32-threads, wasm64, wasm64-threads
./.gitlab/ci/docker/build.sh
```

You can set these environment variables prior to running `build.sh` and `push.sh` to customize a few things that matter most.

1. `VTK_VCS_URL`
    [OPTIONAL] Specify a valid URL that will be used in `git clone` to pull VTK source code. The default is `https://gitlab.kitware.com/vtk/vtk.git`.
2. `VTK_VCS_REF`
    [OPTIONAL] You can build the vtk-wasm image from any branch by providing it's name here. The default builds the `master` branch.
3. `VTK_IMAGE_TAG`
    [OPTIONAL] Specify tag of the image.
4. `VTK_BUILD_ARCHITECTURE`
    [REQUIRED] Specify the build architecture for VTK cross compilation. Possible values: `wasm32`, `wasm32-threads`, `wasm64`, `wasm64-threads`
